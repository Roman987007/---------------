﻿<!DOCTYPE html>
<html>
<head>
	<title>Интернет-магазин "Колеса.ру"</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="font.css">

	<link rel="stylesheet" type="text/css" href="themes/theme1.css" />
	<link rel="stylesheet" type="text/css" href="wicart.css" />
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" ></script>
	<script src="wicart.js"  type="text/javascript" ></script>
</head>
<body>
	
	<header>
	<div class="header-center">
		<a href="#" class="logo"><img src="images/logo.png"></a>
		<p class="logo-name">Колёса.ру</p>

		<ul class="social-link">
			<a href="#" target="_blank"><li class="youtube-link"></li></a>
			<a href="#" target="_blank"><li class="vk-link"></li></a>
			<a href="#" target="_blank"><li class="facebook-link"></li></a>
		</ul>

		<ul class="icon-phone">
			<li class="phone">Многоканальный телефон
			<br>
			<b><span class="number">+7 812 244 46 67</span></b>
		</ul>
		<div class="cart">
<div class="price"><a href="#" id="basketwidjet" onclick="cart.showWinow('bcontainer', 1)"></a></div>

<div style="text-align: center"><a href="#!" onclick="cart.clearBasket()">Очистить корзину</a></div>
</div>

	</div>

	<nav class="nav-bar">
		<ul>
			<li><a href="index.php" >Шины</a></li>
			<li><a href="wheels-disk.php">Диски</a></li>
			<li><a href="Oplata i dostavka.php">Оплата и доставка</a></li>
			<li><a href="tireService.php" class="active">Шиномонтаж</a></li>
			<li><a href="PointsOfIssue.php">Пункты выдачи</a></li>
		</ul>
	</nav>
</header>

<content >
<div class="breadcrumb">
			<a href="/"></a><a href="index.html" title="Главная">Главная</a></a> / Шиномонтаж
		</div>
		<div class="header_center">
			<h3 class = "title_oplata">Шиномонтаж в Пензе</h3>
			<p class="payment_methods">Цены на полный комплекс шиномонтажных работ на 4 колеса</p>
			<table>
			<tbody>
				<tr style="height: 50px">
					<td><b>Легковые:</b></td>
				</tr>
				<tr>
					<td>Полный комплекс 13 - </td>
					<td>900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 14 - </td>
					<td>1200 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 15 - </td>
					<td>1500 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 16 - </td>
					<td >1600 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 17 - </td>
					<td>1700 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 18 - </td>
					<td>1800 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 19 - </td>
					<td>1900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 20 - </td>
					<td>2100 руб.</td>
				</tr>

				<tr style="height: 50px">
					<td><b>Внедорожники:</b></td>
				</tr>
				<tr>
					<td>Полный комплекс 15 - </td>
					<td>1700 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 16 - </td>
					<td>1900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 17 - </td>
					<td>2100 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 18 - </td>
					<td>2300 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 19 - </td>
					<td>2500 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 20 - </td>
					<td>2700 руб.</td>
				</tr>
			</tbody>
			</table>
		</div>
</content>

<form id="formToSend">
<input id="fio" type="text" placeholder="Ваши фамилия и имя"  class="" />
<input id="city" type="text" placeholder="Город"  class="text-input"/>
<input id="phone" type="text" placeholder="Контактный телефон" class="text-input"/>
<input id="email" type="text" placeholder="Электронная почта" class="" />
<br>
<textarea id="question" placeholder="Адрес"></textarea>
</form>
<button onclick="cart.sendOrder('formToSend,overflw,bsum');" href="#">Отправить заказ</button>
</div>

<script>
var cart;
var config;
var wiNumInputPrefID;
$(document).ready(function(){  
    cart = new WICard("cart");
    config = {'clearAfterSend':true, 'showAfterAdd':false};
    
    cart.init("basketwidjet", config);
    
    
});	
document.addEventListener('visibilitychange', function(e) {
cart.init("basketwidjet", config);
}, false);
</script>

</body>
</html>
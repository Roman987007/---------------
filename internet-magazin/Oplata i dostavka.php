<!doctype html>
<html>
<head>
	<title>Интернет-магазин "Колеса.ру"</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="font.css">
	<link rel="stylesheet" type="text/css" href="wicart.css" />
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" ></script>
	<!-- WI-JQ-HEADER -->
	<script src="wicart.js"  type="text/javascript" ></script>
</head>
<body>
<header>
	<div class="header-center">
		<a href="#" class="logo"><img src="images/logo.png"></a>
		<p class="logo-name">Колёса.ру</p>

		<ul class="social-link">
			<a href="#" target="_blank"><li class="youtube-link"></li></a>
			<a href="#" target="_blank"><li class="vk-link"></li></a>
			<a href="#" target="_blank"><li class="facebook-link"></li></a>
		</ul>

		<ul class="icon-phone">
			<li class="phone">Многоканальный телефон
			<br>
			<b><span class="number">+7 812 244 46 67</span></b>
		</ul>

		<div class="cart">
<div class="price"><a href="#" id="basketwidjet" onclick="cart.showWinow('bcontainer', 1)"></a></div>

<div style="text-align: center"><a href="#!" onclick="cart.clearBasket()">Очистить корзину</a></div>
</div>
	</div>

	<nav class="nav-bar">
		<ul>
			<li><a href="index.php" >Шины</a></li>
			<li><a href="wheels-disk.php">Диски</a></li>
			<li><a href="Oplata i dostavka.php" class="active">Оплата и доставка</a></li>
			<li><a href="tireService.php">Шиномонтаж</a></li>
			<li><a href="#">Пункты выдачи</a></li>
		</ul>
	</nav>
</header>
	<content >
	<div class="breadcrumb">
			<a href="/"></a><a href="index.html" title="Главная">Главная</a></a> / Оплата и доставка
		</div>
		<div class="content_header_center">
			<h2 class="title_oplata">Оплата и доставка</h2>
				<div>
					<h1 class="oplata">Оплата</h1>
					<p class="payment_methods"><b>Осуществить оплату заказанного товара Вы можете следующими способами:</b></p>
					<ul class="oplata_dostavka">
					<li>Наличными курьеру</li>
					<li>безналичным способом на основании счета (доставка осуществляется после поступления денежных средств в полном объеме от покупателя на расчетный счет нашего магазина).</li>
					</ul>
				</div>

			<div>
				<h1 class="dostavka">Доставка</h1>
				<ol> 
					<li><b>Диапазон доставки</b></li>
						<ul class="oplata_dostavka">
							<li>Доставка осуществляется в пределах города Пенза и Пензенской.области</li>
							<li>Доставка до покупателя осуществляется не позднее чем через 48 часов после оформления заказа(при условии наличия товара на центральном складе)</li>
						</ul>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2450471.4381400957!2d42.29577535052377!3d53.145403340324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4140e5425fb3fe87%3A0x102a3a583f197b0!2z0J_QtdC90LfQtdC90YHQutCw0Y8g0L7QsdC7Lg!5e0!3m2!1sru!2sru!4v1478334235179" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<li><b>Стоимость доставки</b></li>
						<ul class="oplata_dostavka">
							<li>Стоимость доставки по Пензенской области — 20 руб. за 1 км. </li>
						</ul>
					<li><b>Режим доставки</b></li>
						<ul class="oplata_dostavka">
							<li>доставки осуществляются ежедневно с 11-00 до 21-00 (высокий сезон с 09-00 до 23-00 в выходные и праздники с 11-00 до 21-00)</li>
							<li>Перед выездом со склада водитель-экспедитор по телефону оповещает покупателя о своем приезде в указанный при оформлении заказа интервал времени. По желанию покупателя водитель-экспедитор может осуществить повторный звонок за 1 час или за 2 часа до приезда на адрес доставки.</li>
						</ul>
				</ol>
				<p class="time_dostavka">Ожидание водителем-экспедитором покупателя по адресу составляет не более 20 минут.</p>
			</div>
		</div>

	<div id="order" class="popup">
		<a href="#" onclick="cart.closeWindow('order', 0)" style="float:right">[закрыть]</a>
			<h4>Введите ваши контактные данные</h4>

		<form id="formToSend">
			<input id="fio" type="text" placeholder="Ваши фамилия и имя"  class="" />
			<input id="city" type="text" placeholder="Город"  class="text-input"/>
			<input id="phone" type="text" placeholder="Контактный телефон" class="text-input"/>
			<input id="email" type="text" placeholder="Электронная почта" class="" />
			<br>
		<textarea id="question" placeholder="Адрес"></textarea>
		</form>
		<button onclick="cart.sendOrder('formToSend,overflw,bsum');" href="#">Отправить заказ</button>
		</div>

	<script>
		var cart;
		var config;
		var wiNumInputPrefID;
	$(document).ready(function(){  
 	   cart = new WICard("cart");
 	   config = {'clearAfterSend':true, 'showAfterAdd':false};
	    
	    cart.init("basketwidjet", config);
	    /* WI-MODULES */
	    
		    
		});	
	document.addEventListener('visibilitychange', function(e) {
	cart.init("basketwidjet", config);
	}, false);
</script>

	</content>
	</content>
</body>
</html>
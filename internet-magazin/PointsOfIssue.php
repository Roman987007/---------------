﻿<!DOCTYPE html>
<html>
<head>
	<title>Интернет-магазин "Колеса.ру"</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="font.css">

	<link rel="stylesheet" type="text/css" href="themes/theme1.css" />
	<link rel="stylesheet" type="text/css" href="wicart.css" />
	<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" ></script>
	<script src="wicart.js"  type="text/javascript" ></script>
</head>
<body>

<header>
	<div class="header-center">
		<a href="#" class="logo"><img src="images/logo.png"></a>
		<p class="logo-name">Колёса.ру</p>

		<ul class="social-link">
			<a href="#" target="_blank"><li class="youtube-link"></li></a>
			<a href="#" target="_blank"><li class="vk-link"></li></a>
			<a href="#" target="_blank"><li class="facebook-link"></li></a>
		</ul>

		<ul class="icon-phone">
			<li class="phone">Многоканальный телефон
			<br>
			<b><span class="number">+7 812 244 46 67</span></b>
		</ul>
		<div class="cart">
<div class="price"><a href="#" id="basketwidjet" onclick="cart.showWinow('bcontainer', 1)"></a></div>

<div style="text-align: center"><a href="#!" onclick="cart.clearBasket()">Очистить корзину</a></div>
</div>

	</div>

	<nav class="nav-bar">
		<ul>
			<li><a href="index.php">Шины</a></li>
			<li><a href="wheels-disk.php">Диски</a></li>
			<li><a href="Oplata i dostavka.php">Оплата и доставка</a></li>
			<li><a href="tireService.php">Шиномонтаж</a></li>
			<li><a href="PointsOfIssue.php" class="active">Пункты выдачи</a></li>
		</ul>
	</nav>
</header>

<content >
		<div class="breadcrumb">
			<a href="/"></a><a href="index.html" title="Главная">Главная</a></a> / Пункты выдачи
		</div>
		<div class="content_header_center">
		<h3 class = "title_oplata">Пункты выдачи</h3>

			<div class="PointsOfIssue">
				<p>Мы находимся по адресам:</p>
				<div class="address-block">
					<p>г.Пенза, ул.Пушкина д.137, к.1</p>
					<p><b>Телефон:</b>
					+7(8412)54-00-28</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
				<div class="address-block">
					<p>г.Пенза, ул.Московская д.73</p>
					<p><b>Телефон:</b>
					+7(8412)75-75-85</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
				<div class="address-block">
					<p>г.Пенза, ул.Рахманинова д.1</p>
					<p><b>Телефон:</b>
					+7(8412)50-50-20</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
			</div>

			<div id="maps">
				<iframe src="https://api-maps.yandex.ru/frame/v1/-/CZsgUJOJ" width="560" height="400" frameborder="0"></iframe>
			</div>
		</div>
</content>

<div id="order" class="popup">
<a href="#" onclick="cart.closeWindow('order', 0)" style="float:right">[закрыть]</a>
<h4>Введите ваши контактные данные</h4>

<form id="formToSend">
<input id="fio" type="text" placeholder="Ваши фамилия и имя"  class="" />
<input id="city" type="text" placeholder="Город"  class="text-input"/>
<input id="phone" type="text" placeholder="Контактный телефон" class="text-input"/>
<input id="email" type="text" placeholder="Электронная почта" class="" />
<br>
<textarea id="question" placeholder="Адрес"></textarea>
</form>
<button onclick="cart.sendOrder('formToSend,overflw,bsum');" href="#">Отправить заказ</button>
</div>

<script>
var cart;
var config;
var wiNumInputPrefID;
$(document).ready(function(){  
    cart = new WICard("cart");
    config = {'clearAfterSend':true, 'showAfterAdd':false};
    
    cart.init("basketwidjet", config);
    
    
});	
document.addEventListener('visibilitychange', function(e) {
cart.init("basketwidjet", config);
}, false);
</script>


</body>
</html>
<?php
include("include/db_connect.php");
?>
<!doctype html>
<html>
<head>
	<title>Интернет-магазин "Колеса.ру"</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="font.css">
	    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
<header>
	<div class="header-center">
		<a href="#" class="logo"><img src="images/logo.png"></a>
		<p class="logo-name">Колёса.ру</p>

		<ul class="social-link">
			<a href="#" target="_blank"><li class="youtube-link"></li></a>
			<a href="#" target="_blank"><li class="vk-link"></li></a>
			<a href="#" target="_blank"><li class="facebook-link"></li></a>
		</ul>

		<ul class="icon-phone">
			<li class="phone">Многоканальный телефон
			<br>
			<b><span class="number">+7 812 244 46 67</span></b>
		</ul>
		<p align="right" id="block-basket"><a href="cart.php?action=oneclick">Корзина пуста</a></p>
	</div>

	<nav class="nav-bar">
		<ul>
			<li><a href="index.php" class="active">Шины и Диски</a></li>
			<li><a href="Oplata i dostavka.php">Оплата и доставка</a></li>
			<li><a href="tireService.php">Шиномонтаж</a></li>
			<li><a href="PointsOfIssue.php">Пункты выдачи</a></li>
		</ul>
	</nav>
</header>
	<content >
		<div class="content_header_center">
				<?php

				$result = mysql_query("SELECT * FROM `product` WHERE 1", $link);
				if(mysql_num_rows($result) > 0){
					$rows = mysql_fetch_array($result);
					do{
						echo '
							<div class="label">
								<div class="picture">
									<img src="/upload_images/'.$rows["IMAGE"].' " alt="'.$rows["ID"].'" />
								</div>
									<div class="title">
										<p>'.$rows["NAME"].'</p>
									</div>
								<div class="product-tile-checkout-section">
									<div class="product-price"><strong>'.$rows["PRICE"].' ₽</strong></div>
										<div class="nal">
											<p>✓ В наличии</p>
										</div>
								</div>
								<a class="add-cart-style-list" ></a>
								<!--<a class="add-cart-style-list" tovarid=" '.$rows["ID"].' "></a>-->
							</div>
						';
				}
					while ($rows = mysql_fetch_array($result));
				}
			?>
			
		</div>
	</content>

</body>
</html>
